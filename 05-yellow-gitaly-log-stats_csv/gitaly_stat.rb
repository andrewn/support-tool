require 'descriptive_statistics'

data = File.read(ARGV[0]).split("\n")

timings = Hash.new([])

data.each do |line|
  if line =~ /grpc.method=(\w*)/
    method = $1
  end

  if line =~ /grpc.time_ms=([\d\.]*)/
    timings[method] += [$1.to_f]
  end
end

def standard_deviation(list)
  mean = list.inject(:+) / list.length.to_f
  var_sum = list.map{|n| (n-mean)**2}.inject(:+).to_f
  sample_variance = var_sum / (list.length - 1)
  Math.sqrt(sample_variance)
end

data = []
timings.each do |key, values|
  p99 = values.percentile(99).round(2)
  score = values.count * p99

  data += [{ method: key,
             max: values.max,
             min: values.min,
             stddev: standard_deviation(values).round(2),
             p95: values.percentile(95).round(2),
             p99: p99,
             count: values.count,
             score: score.round(1) }]
end

sorted = data.sort_by! { |x| x[:score] }.reverse

puts "#Method,Max,Min,Stddev,Count,P95,P99,Score"

sorted.each do |values|
  puts "#{values[:method]},#{values[:max]},#{values[:min]},#{values[:stddev]},#{values[:count]},#{values[:p95]},#{values[:p99]},#{values[:score]}"
end
