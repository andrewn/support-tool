# support-tool

This is a proof-of-concept of a generic support tool for collecting statistics from a GitLab instance, for the purpose of 
diagnosing issues, particularly performance issues. 

# Using 

```shell
$ sudo ./run 
Executing 01-orange-system-info
No LSB modules are available.
Executing 02-yellow-home-directory-partition-type
Executing 03-yellow-perf-scheduler-latency
[ perf record: Woken up 1 times to write data ]
[ perf record: Captured and wrote 0.141 MB /tmp/tmp.0c5ZWQ8DCf-perf-scheduler-latency (~6161 samples) ]
Executing 04-yellow-iosnoop
+ tar -cvzf report-20190205-131453.tgz report-20190205-131453/
report-20190205-131453/
report-20190205-131453/01-orange-system-info.txt
report-20190205-131453/02-yellow-home-directory-partition-type.txt
report-20190205-131453/03-yellow-perf-scheduler-latency.txt
report-20190205-131453/04-yellow-iosnoop.txt
+ echo 'Report created in report-20190205-131453.tgz'
Report created in report-20190205-131453.tgz

$ cat report-20190205-131453/01-orange-system-info.txt
# uname -a
Linux vagrant-ubuntu-trusty-64 3.13.0-160-generic #210-Ubuntu SMP Mon Sep 24 18:08:15 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
# lsb_release -a
Distributor ID:	Ubuntu
Description:	Ubuntu 14.04.5 LTS
Release:	14.04
Codename:	trusty

$ cat report-20190205-131453/02-yellow-home-directory-partition-type.txt
# /home/git directory should not be mounted on nfs
Filesystem     Type  Size  Used Avail Use% Mounted on
/dev/sda1      ext4   40G  1.5G   37G   4% /

$ cat report-20190205-131453/03-yellow-perf-scheduler-latency.txt
Scheduler latencies for 10s, starting at ~3830.95s

 ---------------------------------------------------------------------------------------------------------------
  Task                  |   Runtime ms  | Switches | Average delay ms | Maximum delay ms | Maximum delay at     |
 ---------------------------------------------------------------------------------------------------------------
  perf:9008             |      0.800 ms |        2 | avg:    0.318 ms | max:    0.633 ms | max at: 3835.361865 s
  vminfo:1162           |      1.885 ms |        7 | avg:    0.089 ms | max:    0.458 ms | max at: 3841.510887 s
  vmstats:1165          |      0.412 ms |        4 | avg:    0.070 ms | max:    0.192 ms | max at: 3845.236453 s
  automount:1166        |      1.472 ms |       19 | avg:    0.042 ms | max:    0.243 ms | max at: 3843.906483 s
  rcuos/0:8             |      0.286 ms |        9 | avg:    0.034 ms | max:    0.277 ms | max at: 3836.499339 s
  kworker/u2:2:126      |      0.116 ms |        2 | avg:    0.027 ms | max:    0.041 ms | max at: 3845.265879 s
  kworker/u3:0:19       |      0.099 ms |        3 | avg:    0.027 ms | max:    0.055 ms | max at: 3841.222884 s
  kworker/0:1:25        |      2.150 ms |       17 | avg:    0.027 ms | max:    0.081 ms | max at: 3841.220420 s
  memballoon:1164       |      0.135 ms |        2 | avg:    0.026 ms | max:    0.034 ms | max at: 3835.570648 s
  cron:1110             |      0.079 ms |        1 | avg:    0.024 ms | max:    0.024 ms | max at: 3843.535425 s
  ksoftirqd/0:3         |      0.094 ms |        3 | avg:    0.022 ms | max:    0.039 ms | max at: 3840.211262 s
  sleep:9011            |      0.841 ms |        3 | avg:    0.020 ms | max:    0.033 ms | max at: 3845.375768 s
  watchdog/0:12         |      0.000 ms |        3 | avg:    0.019 ms | max:    0.020 ms | max at: 3844.531935 s
  timesync:1161         |      0.250 ms |        2 | avg:    0.017 ms | max:    0.025 ms | max at: 3844.751934 s
  jbd2/sda1-8:174       |      0.119 ms |        3 | avg:    0.014 ms | max:    0.019 ms | max at: 3841.223533 s
  khungtaskd:27         |      0.049 ms |        1 | avg:    0.010 ms | max:    0.010 ms | max at: 3844.750812 s
  rcu_sched:7           |      0.106 ms |       12 | avg:    0.008 ms | max:    0.048 ms | max at: 3836.500362 s
  dbus-daemon:888       |      0.614 ms |        2 | avg:    0.008 ms | max:    0.011 ms | max at: 3836.499181 s
 -----------------------------------------------------------------------------------------
  TOTAL:                |      9.507 ms |       95 |
 ---------------------------------------------------

$ cat report-20190205-131453/04-yellow-iosnoop.txt
# iosnoop 10
Tracing block I/O for 10 seconds (buffered)...
COMM         PID    TYPE DEV      BLOCK        BYTES     LATms
jbd2/sda1-17 174    WS   8,0      1050712      45056      0.31
<idle>       0      WS   8,0      1050800      4096       0.23
kworker/u2:2 126    W    8,0      2048         8192       6.02
kworker/u2:2 126    WM   8,0      3208         4096       6.01
kworker/u2:2 126    WM   8,0      4016         4096       6.00
kworker/u2:2 126    WM   8,0      4320         4096       5.99
kworker/u2:2 126    WM   8,0      7432         4096       5.99
kworker/u2:2 126    WM   8,0      31192        4096       5.98
kworker/u2:2 126    WM   8,0      32504        4096       5.98
kworker/u2:2 126    WM   8,0      52664        4096       5.97
kworker/u2:2 126    WM   8,0      114232       4096       5.97
kworker/u2:2 126    WM   8,0      127416       4096       5.97

Ending tracing...
```

# Notes

1. Each script includes a categorization according to our [Data Classification Policy](https://docs.google.com/document/d/15eNKGA3zyZazsJMldqTBFbYMnVUSQSpU14lo22JMZQY/edit#)
2. In future, we should be able to limit scripts by data categorization
3. Individual scripts can fail without affecting the overall success of the tool. For example, some scripts may rely on `perf`, but will fail if it is not available. The tool will handle this elegantly.